# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
from tif_picture_to_dataframe import *
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import seaborn as sns



def RF_train(path_to_train_csv):
    """Train the random forest classifier using ground truth"""
    print("Starting Random Forest Classifier training...")
    features  = pd.read_csv(path_to_train_csv, delimiter=',', encoding="utf-8-sig") #open the train file
    
    #LABELS = what we want to predict
    labels = np.array(features["specie"]) 
    #FEATURES = useful data for algorithm
    features = features.drop(["specie", "row", "col", "treeid", "family", "plotid"], axis = 1) 

    

    features = np.array(features) #convert to numpy array to be used by sklearn
    
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size = 0.1, random_state=42)

    ###PRINCIPAL COMPONENT ANALYSIS
    
    scaler = StandardScaler()

    scaler.fit(X_train)  # Fit on training set only.
    X_train = scaler.transform(X_train) # Apply transform to both the training set and the test set.
    X_test = scaler.transform(X_test)

    pca = PCA(0.999) #empirically determined variance for the best results
    pca.fit(X_train)
    
    #actually applies PCA on the features
    X_train = pca.transform(X_train)
    X_test = pca.transform(X_test)


    ###RANDOM FOREST CLASSIFIER
    rf = RandomForestClassifier(n_estimators = 100, random_state = 42) #random_state = 42 guarantees that the results will be the same
    rf.fit(X_train, y_train)
    
    predictions = rf.predict(X_test) 
    
    print("Random Forest Classifier training : done.")
    print("RFC Accuracy: {}%".format(round(100*metrics.accuracy_score(y_test, predictions), 1))) #print accuracy
    
    
    
    return rf, pca, y_test, predictions #to be used in other functions

def cumulative_explained_variance():
    
    pca = RF_train("data_train_full.csv")[1]
    
    
    plt.plot(np.cumsum(pca.explained_variance_ratio_))
    plt.xlabel('number of components')
    plt.ylabel('cumulative explained variance');
    plt.show()
    
def display_confusion_matrix():
    
    RandomForest = RF_train(path_to_train_csv)
    
    y_test = RandomForest[2]
    predictions = RandomForest[3]
    
    liste_especes = ['ABAL', 'ACPS', 'BEPE', 'BEsp', 'COAV', 'FASY', 'FREX', 'PIAB', 'PICE', 'PIUN', 'POTR', 'SOAR', 'SOAU']
    cm=metrics.confusion_matrix(y_test,predictions)

    df_cm = pd.DataFrame(cm, index = [i for i in liste_especes], columns = [i for i in liste_especes])

    sns.heatmap(df_cm, annot=True)
    
def display_features_importance():   
    RandomForest = RF_train("data_train_full.csv")
    rf = RandomForest[0]
    feature_list = RandomForest[4]
    feature_imp = pd.Series(rf.feature_importances_, index=feature_list).sort_values(ascending=False)

    # Creating a bar plot
    sns.barplot(x=feature_imp, y=feature_imp.index)
    #Add labels to graph
    plt.xlabel('Feature Importance Score')
    plt.ylabel('Features')
    plt.title("Visualizing Important Features")
    plt.legend()
    plt.show()
    

def RF_predictions(name_of_picture):
    """Use Random Forest on a picture"""
    
    print("Starting predictions on given picture...")
    
    df = add_lidar_data(name_of_picture)
    df = df.drop(["row", "col", "plotid"], axis=1)
    
    X = np.array(df)
    
    ###PRINCIPAL COMPONENT ANALYSIS
    
    scaler = StandardScaler()

    scaler.fit(X)  # Fit on training set only.
    X = scaler.transform(X) # Apply transform to both the training set and the test set.

    
    rf = RF_train("data_train_full.csv")
    
    X = rf[1].transform(X)
    predictions = rf[0].predict(X)
    
    
    print("Predictions : done.")
    
    return np.array(predictions)
    
def discrete_matshow(data):
    print("Plotting the species map...")
    #get discrete colormap
    cmap = plt.get_cmap('tab20', np.max(data)-np.min(data)+1)
    # set limits .5 outside true range
    mat = plt.matshow(data,cmap=cmap,vmin = np.min(data)-.5, vmax = np.max(data)+.5)
    #tell the colorbar to tick at integers
    cax = plt.colorbar(mat, ticks=np.arange(np.min(data),np.max(data)+1))
    #name of the species instead of numbers
    
    cax.ax.set_yticklabels(['ABAL', 'ACPS', 'BEPE', 'BEsp', 'COAV', 'FASY', 'FREX', 'PIAB', 'PICE', 'PIUN', 'POTR', 'SOAR', 'SOAU' ]) 

    print("Plotting : done.")
    
    
def plot_species_map(image_name):
    """Plot heatmap showing to which species a pixel belongs"""
    
    predictions = RF_predictions(image_name)


    hi_tiff = tifffile.TiffFile(r"hi\{}.tif".format(image_name))
    hi_arr = hi_tiff.asarray()
    hi_metadata = hi_tiff.shaped_metadata[0]
    
    n_cols = hi_metadata["ncols"]
    n_rows = hi_metadata["nrows"]


    predictions = np.reshape(predictions, (n_rows, n_cols))
    
    discrete_matshow(predictions)
    
    
    

plot_species_map("1")















